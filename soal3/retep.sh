#!/bin/bash

echo "Login"

# Input username
read -p "Username: " username

# Cek informasi user di dalam file /users/users.txt
user_info=$(grep "^$username:" ./users/users.txt)

# Cek apakah ada username yang sama antara input dengan yang ada di file /users/users.txt dan juga mengambil passwrod
if [[ -n "$user_info" ]]; then
  password=$(echo "$user_info" | cut -d ":" -f 2)
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
fi

# Input password
read -s -p "Password: " input_password
echo

# Cek apakah password input sama dengan password  yang ada di file /users/users.txt
if [[ "$input_password" == "$password" ]]; then
  echo "Login successful"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
fi
