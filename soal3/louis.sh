#!/bin/bash

echo "Register new user"

# Input username
read -p "Username: " username

# Cek apakah username sudah terdaftar atau belum
if grep -q "^$username:" ./users/users.txt; then
  echo "Username already exists."
  echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
  exit 1
fi

# Input password
while true; do
  read -s -p "Password: " password
  echo

  # Cek syarat password
  if [[ ${#password} -ge 8 && "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] && "$password" != "$username" && "$password" != "chicken" && "$password" != "ernie" ]]; then
    break
  else
    echo "Please try again, your password not strong enough."
  fi
done

# Input informasi user ke dalam file /users/users.txt
echo "$username:$password" >> ./users/users.txt

echo "User registered successfully"
echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
