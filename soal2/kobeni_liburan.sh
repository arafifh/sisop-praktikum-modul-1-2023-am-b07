#!/bin/bash

# scrape indonesia images from freeimages.com
curl -o output.txt https://www.freeimages.com/search/indonesia

# text filtering to get images url
grep "images.free" output.txt | sed "s/.* data-src=\"\(.*\)\".*/\1/" | sed "s/.* src=\"\(.*\)\".*/\1/" > list.txt

times=$(date '+%H:%M:%S')
times=$( printf '%d' $times ) # convert str to int

b=1

dir='/home/rep/Project/Bash/Shift-1' # base dir

# make dir
while [ -d "$dir/kumpulan_$b" ]
do
    b=$((b+1))
done

while [ ! -d "$dir/kumpulan_$b" ]
do
    mkdir $dir/kumpulan_$b
done

echo -e "\nCurrent time : $(date +%H:%M:%S)"

# case if time = 0 or > 0
if [ ${times:0:2} -eq 0 ]
then
    a=1
    echo -e "1 files will be downloaded.\n"

    # download random files about indonesia
    wget --no-check-certificate -O $dir/kumpulan_$b/perjalanan_$a $(shuf -n 1 list.txt)

else
    a=0
    echo -e "${times:0:2} files will be downloaded for every 10 hours.\n"

    while [ $a -lt $times ]
    do
        a=$((a + 1))

        # download random files about indonesia
        wget --no-check-certificate -O $dir/kumpulan_$b/perjalanan_$a $(shuf -n 1 list.txt)

        if [ $a -eq $times ]
        then
            sleep 0
        else
            echo "Wait for 10 hours to the next file to be downloaded."
            sleep 36000
        fi
    done
fi

echo "$a files has been downloaded successfully in $dir/kumpulan_$b"
echo "Zipping each directory .."

for ((i=1; i<=b; i++))
do
    zip -r $dir/devil_$i.zip $dir/kumpulan_$i
    sleep 86400
done
