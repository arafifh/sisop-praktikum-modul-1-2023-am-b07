# Nama : Charles
# NRP : 5025211082
# Kelas : B

# Asumsi file .csv berada dalam direktory yang sama dengan file .sh
# jika tidak dalam direktori sama, file path bisa sesuai dengan lokasi dari file 
# kemudian ganti semua '2023 QS World University Rankings.csv' dengan "$FILE_PATH"
# FILE_PATH="/home/$USER/Desktop/2023 QS World University Rankings.csv"


#1 Tampilkan 5 universitas dengan ranking tertinggi di jepang
echo "No. 1"
echo "Berikut adalah 5 universitas dengan ranking tertinggi di Jepang : "
awk -F ',' '$4 == "Japan" { print }' '2023 QS World University Rankings.csv' | head -n 5
echo ""

#2 Cari universitas dengan fsr score terendah dari hasil filter soal pertama
echo "No. 2"
echo "Universitas dengan Faculty Student Score terendah : "
awk -F ',' '$4 == "Japan" { print }' '2023 QS World University Rankings.csv' | sort -t "," -g  -k 9,9 | head -n 5
echo ""

#3 Cari 10 universitas di jepang dengan employment outcome rank paling tinggi
echo "No. 3"
echo "Top 10 Universitas di Jepang dengan Employment Outcome Rank paling tinggi : "
awk -F ',' '$4 == "Japan" { print }' '2023 QS World University Rankings.csv' | awk -F ',' -v OFS=',' '$20 == "601+" {$20 = "601"}1' | sort -t "," -g -k 20,20 | head -n 10
echo ""

#4 Cari universitas dengan kata kunci keren
echo "No. 4"
echo "Universitas yang memiliki kata keren"
awk -F ',' '$2 ~ /Keren/ { print }' '2023 QS World University Rankings.csv' 
