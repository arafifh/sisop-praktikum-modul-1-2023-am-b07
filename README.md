# sisop-praktikum-modul-1-2023-AM-B07





## Soal 1

Kode : 

```
#1 Tampilkan 5 universitas dengan ranking tertinggi di jepang
echo "No. 1"
echo "Berikut adalah 5 universitas dengan ranking tertinggi di Jepang : "
awk -F ',' '$4 == "Japan" { print }' '2023 QS World University Rankings.csv' | head -n 5
echo ""

#2 Cari universitas dengan fsr score terendah dari hasil filter soal pertama
echo "No. 2"
echo "Universitas dengan Faculty Student Score terendah : "
awk -F ',' '$4 == "Japan" { print }' '2023 QS World University Rankings.csv' | sort -t "," -g  -k 9,9 | head -n 5
echo ""

#3 Cari 10 universitas di jepang dengan employment outcome rank paling tinggi
echo "No. 3"
echo "Top 10 Universitas di Jepang dengan Employment Outcome Rank paling tinggi : "
awk -F ',' '$4 == "Japan" { print }' '2023 QS World University Rankings.csv' | awk -F ',' -v OFS=',' '$20 == "601+" {$20 = "601"}1'>
echo ""

#4 Cari universitas dengan kata kunci keren
echo "No. 4"
echo "Universitas yang memiliki kata keren"
awk -F ',' '$2 ~ /Keren/ { print }' '2023 QS World University Rankings.csv' 

```

### Poin A

Diminta untuk mencari 5 universitas dengan ranking tertinggi di Jepang. Pertama-tama kita dapat mengsortir dulu universitas mana yang berada di Jepang.

`awk -F ',' '$4 == "Japan" { print }' '2023 QS World University Rankings.csv'`

Karena kolom asal universitas berada di kolom 4, kita dapat membuat `'$4 == "Japan"`
yang memiliki arti mencari kolom mana yang berisi value "Japan" jika ketemu, kita print.

`'2023 QS World University Rankings.csv'` merupakan file yang ingin di buka.

Setelah itu dapat kita pipelinekan dengan `|` sehingga output dari perintah di atas akan menjadi
input command selanjutnya. Command selanjutnya yakni `head -n 5` mengambil 5 row teratas dari hasil output tadi. 
Hal ini bisa dilakukan karena file csv tersebut sudah tersortir berdasarkan ranking

Hasil run : 

![Nomor 1a](soal1/Nomor%201a.png)

### Poin B
Diminta untuk mencari 5 universitas dengan FSR score terendah. Pertama kita dapat mengfilter university yang ada di jepang terlebih dahulu.

`awk -F ',' '$4 == "Japan" { print }' '2023 QS World University Rankings.csv'`

Setelah itu kita dapat mempipe lagi dengan command berikut

`sort -t "," -g  -k 9,9 | head -n 5`

Command tersebut akan mengsortir berdasarkan kolom ke 9, kemudian mengambil row teratas.

Hasil run : 

![Nomor 1b](soal1/Nomor%201b.png)

### Poin C
Diminta untuk mencari 10 universitas di Jepang dengan rank employment outcome paling tinggi. Pertama kita menjalankan command 

`awk -F ',' '$4 == "Japan" { print }' '2023 QS World University Rankings.csv'`

untuk mendapatkan universitas yang berada di Jepang. Setelah itu jalankan command berikut dengan pipe 

`awk -F ',' -v OFS=',' '$20 == "601+" {$20 = "601"}1'`
Command tersebut akan mengubah semua nilai di kolom ke 20 yang bernilai 601+ menjadi 601. Hal ini dilakukan agar sortir nanti dapat dilakukan. Setelah itu kita bisa sortir

 `sort -t "," -g -k 20,20 | head -n 10`

Command tersebut akan mengsort berdasarkan kolom ke 20 dan menampilkan 10 row teratas.

Hasil run : 

![Nomor 1c](soal1/Nomor%201c.png)

### Poin D
Diminta untuk mencari universitas yang memiliki kata keren. Kita perlu menggunakan command awk untuk mencari kolom nama yang memiliki kata keren

`awk -F ',' '$2 ~ /Keren/ { print }' '2023 QS World University Rankings.csv'`

Command tersebut akan mencari di kolom nama universitas, nama yang mengandung kata `Keren`.

Hasil run : 

![Nomor 1d](soal1/Nomor%201d.png)



## Soal 2
Kode :
```
# scrape indonesia images from freeimages.com
curl -o output.txt https://www.freeimages.com/search/indonesia

# text filtering to get images url
grep "images.free" output.txt | sed "s/.* data-src=\"\(.*\)\".*/\1/" | sed "s/.* src=\"\(.*\)\".*/\1/" > list.txt

times=$(date '+%H:%M:%S')
times=$( printf '%d' $times ) # convert str to int

b=1

dir='/home/rep/Project/Bash/Shift-1' # base dir

# make dir
while [ -d "$dir/kumpulan_$b" ]
do
    b=$((b+1))
done

while [ ! -d "$dir/kumpulan_$b" ]
do
    mkdir $dir/kumpulan_$b
done

echo -e "\nCurrent time : $(date +%H:%M:%S)"

# case if time = 0 or > 0
if [ ${times:0:2} -eq 0 ]
then
    a=1
    echo -e "1 files will be downloaded.\n"

    # download random files about indonesia
    wget --no-check-certificate -O $dir/kumpulan_$b/perjalanan_$a $(shuf -n 1 list.txt)

else
    a=0
    echo -e "${times:0:2} files will be downloaded for every 10 hours.\n"

    while [ $a -lt $times ]
    do
        a=$((a + 1))

        # download random files about indonesia
        wget --no-check-certificate -O $dir/kumpulan_$b/perjalanan_$a $(shuf -n 1 list.txt)

        if [ $a -eq $times ]
        then
            sleep 0
        else
            echo "Wait for 10 hours to the next file to be downloaded."
            sleep 36000
        fi
    done
fi

echo "$a files has been downloaded successfully in $dir/kumpulan_$b"
echo "Zipping each directory .."

for ((i=1; i<=b; i++))
do
    zip -r $dir/devil_$i.zip $dir/kumpulan_$i
    sleep 86400
done
```
Di soal ini, kita diminta untuk membuat program yang dapat mendownload gambar tentang Indonesia secara otomatis selama 10 jam sekali sebanyak X. X ini ditentukan oleh jam saat ini. Misal saat ini pukul 16:09, maka download gambar sebanyak 16 kali. Gambar yang didownload akan diberikan format nama `perjalanan_NOMOR`. Setelah itu, gambar akan dimasukkan ke dalam direktori `kumpulan_NOMOR`. NOMOR pada direktori ini akan di-increment setiap program dijalankan. Setelah itu akan dilakukan zipping pada setiap direktori.

Yang pertama kita lakukan adalah `scraping` website gambar-gambar. Dalam contoh ini adalah `freeimages.com`. `Scraping` dilakukan dengan cURL dan hasilnya dimasukkan ke dalam output.txt.
```
curl -o output.txt https://www.freeimages.com/search/indonesia
```
Setelah itu kita lakukan `grep` untuk mengambil element dengan url tertentu.
```
grep "images.free" output.txt
```
Kode tersebut akan memunculkan hasil yang berantakan seperti ini.
```
src="https://images.freeimages.com/images/grids/6e8/a-calm-day-in-cimahi-waterfall-west-java-indonesia-1640584.jpg" data-src="https://images.freeimages.com/images/previews/6e8/a-calm-day-in-cimahi-waterfall-west-java-indonesia-1640584.jpg"
src="https://images.freeimages.com/images/grids/996/a-beautiful-beach-in-nusa-penida-island-bali-indonesia-1640590.jpg" data-src="https://images.freeimages.com/images/previews/996/a-beautiful-beach-in-nusa-penida-island-bali-indonesia-1640590.jpg" 
```
Masih terdapat atribut HTML seperti `src` dan `data-src`. Kita dapat gunakan `sed` untuk filtering text dan mengambil isi dari `src` dan `data-src` saja lalu masukkan ke list.txt. Sehingga kode lengkapnya akan jadi seperti ini.
```
grep "images.free" output.txt | sed "s/.* data-src=\"\(.*\)\".*/\1/" | sed "s/.* src=\"\(.*\)\".*/\1/" > list.txt
```
Kita buat variabel times untuk menampung waktu saat ini. Lalu kita konversi dari `string` ke `integer`. Lalu, nanti akan kita lakukan `slicing` agar mendapatkan jamnya saja.
```
times=$(date '+%H:%M:%S')
times=$( printf '%d' $times )
```
Sebelum kita memulai ke proses download. Kita buat dahulu direktori `kumpulan_b`. Disini `NOMOR` diidentifikasi sebagai `b`. Kita lakukan checking dengan loop, jika direktori `kumpulan_b` sudah ada, maka `b` akan di-increment. Jika belum ada, maka akan dibuat.
```
while [ -d "$dir/kumpulan_$b" ]
do
    b=$((b+1))
done

while [ ! -d "$dir/kumpulan_$b" ]
do
    mkdir $dir/kumpulan_$b
done
```
Lalu, kita cek apakah jamnya 00 atau lebih. Jika jamnya lebih dari 00, maka akan terjadi loop sebanyak times dan lakukan download gambar menggunakan wget dengan parameter -O `kumpulan_$b/perjalanan_$a` yang mana hasil download akan dimasukkan ke dalam direktori tersebut.
Di sini kita gunakan `sleep` selama 36000 detik yang artinya jeda selama 10 jam.
```
while [ $a -lt $times ]
do
    a=$((a + 1))

    # download random files about indonesia
    wget --no-check-certificate -O $dir/kumpulan_$b/perjalanan_$a $(shuf -n 1 list.txt)

    if [ $a -eq $times ]
    then
        sleep 0
    else
        echo "Wait for 10 hours to the next file to be downloaded."
        sleep 36000
    fi
done
```
command `shuf -n 1 list.txt` berarti mengambil 1 baris pada list.txt secara acak. Sehingga kita dapat mendownload gambar secara acak.

Setelah itu, kita lakukan zipping pada setiap direktori. Kita gunakan `sleep` selama 86400 detik atau 24 jam.
```
for ((i=1; i<=b; i++))
do
    zip -r $dir/devil_$i.zip $dir/kumpulan_$i
    sleep 86400
done
```
Parameter `-r` pada command zip di atas adalah lakukan zipping secara rekursif, maksudnya isi dari direktori akan dimasukkan ke dalam zip juga.

Hasil run:

![Output](soal2/Screenshot_from_2023-03-10_19-18-13.png)
![Hasil run 1](soal2/Screenshot_from_2023-03-10_19-20-12.png)
![Hasil run 2](soal2/Screenshot_from_2023-03-10_19-20-20.png)

## Soal 3
Kode louis.sh :
```
#!/bin/bash

echo "Register new user"

# Input username
read -p "Username: " username

# Cek apakah username sudah terdaftar atau belum
if grep -q "^$username:" ./users/users.txt; then
  echo "Username already exists."
  echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
  exit 1
fi

# Input password
while true; do
  read -s -p "Password: " password
  echo

  # Cek syarat password
  if [[ ${#password} -ge 8 && "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] && "$password" != "$username" && "$password" != "chicken" && "$password" != "ernie" ]]; then
    break
  else
    echo "Please try again, your password not strong enough."
  fi
done

# Input informasi user ke dalam file /users/users.txt
echo "$username:$password" >> ./users/users.txt

echo "User registered successfully"
echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
```

Kode retep.sh : 
```
#!/bin/bash

echo "Login"

# Input username
read -p "Username: " username

# Cek informasi user di dalam file /users/users.txt
user_info=$(grep "^$username:" ./users/users.txt)

# Cek apakah ada username yang sama antara input dengan yang ada di file /users/users.txt dan juga mengambil passwrod
if [[ -n "$user_info" ]]; then
  password=$(echo "$user_info" | cut -d ":" -f 2)
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
fi

# Input password
read -s -p "Password: " input_password
echo

# Cek apakah password input sama dengan password  yang ada di file /users/users.txt
if [[ "$input_password" == "$password" ]]; then
  echo "Login successful"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
fi
```

Ide dari soal ini adalah pertama membuat sebuah file dengan nama louis.sh untuk melakukan register. Pertama ambil sebuah input dan masukkan ke dalam variabel `username`.
```
# Input username
read -p "Username: " username
```
Lalu lakukan cek apakah username sudah terdaftar atau belum di dalam file users.txt.
```
if grep -q "^$username:" ./users/users.txt; then
  echo "Username already exists."
  echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
  exit 1
fi
```
Lalu, lakukan input password dan cek syarat password (minimal 8 karakter, memiliki minimal 1 huruf kapital dan 1 huruf kecil alphanumeric, tidak boleh sama dengan username, tidak boleh menggunakan kata chicken atau ernie)
```
while true; do
  read -s -p "Password: " password
  echo

  # Cek syarat password
  if [[ ${#password} -ge 8 && "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] && "$password" != "$username" && "$password" != "chicken" && "$password" != "ernie" ]]; then
    break
  else
    echo "Please try again, your password not strong enough."
  fi
done
```
Setelah itu, masukkan informasi user ke dalam file users.txt dan masukkan output ke dalam file log.txt dengan format yang sudah ditentukan.
```
echo "$username:$password" >> ./users/users.txt

echo "User registered successfully"
echo "$(date +'%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
```

Pada file retep.sh, kita pertama input username, lalu cek informasi user dalam file users.txt
```
# Input username
read -p "Username: " username

# Cek informasi user di dalam file /users/users.txt
user_info=$(grep "^$username:" ./users/users.txt)
```
Lakukan pengecekan apakah username ada yang sama dengan file users.txt.
```
# Cek apakah ada username yang sama antara input dengan yang ada di file /users/users.txt dan juga mengambil passwrod
if [[ -n "$user_info" ]]; then
  password=$(echo "$user_info" | cut -d ":" -f 2)
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
fi
```
Lalu, kita minta input password. Lalu, lakukan cek apakah ada password yang sama dengan password yang ada di users.txt.
```
# Cek apakah password input sama dengan password  yang ada di file /users/users.txt
if [[ "$input_password" == "$password" ]]; then
  echo "Login successful"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
else
  echo "Invalid username or password"
  echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
fi
```
Semua output yang muncul dalam terminal dimasukkan ke dalam file log.txt.

Hasil run :

![output louis](soal3/Screenshot_from_2023-03-10_21-44-18.png)
![output retep](soal3/Screenshot_from_2023-03-10_21-44-23.png)



## Soal 4

Kode : 


```
# Mendapatkan nama current user
cur_user=$(whoami)

# Format nama
format=$(date +"%H:%M %d:%m:%Y").txt

# Copy log ke dalam temp.txt dan ubah permission
cp /var/log/syslog /home/$cur_user/Desktop/temp.txt
chmod 777 /home/$cur_user/Desktop/temp.txt

# Ambil jam sekarang
rotate=$(date +"%-H")

# String untuk manipulasi
low=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
upp=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ

# Manipulasi string berdasarkan jam kemudian masukin ke file dengan format nama yang ditentukan
tr "${low:0:26}" "${low:${rotate}:26}" < /home/$cur_user/Desktop/temp.txt | tr "${upp:0:26}" "${upp:${rotate}:26}" > /home/$cur_user/Desktop/"$format"

# Hapus temp.txt
rm /home/$cur_user/Desktop/temp.txt

```

Ide dari soal keempat adalah untuk mengambil log dari `var/log/syslog` kemudian di encrypt berdasarkan jam sekarang. Setelah itu kita mengsave file tersebut dengan format "HH:MM DD:MM:YYYY.txt" yang berupa waktu dan tanggal sekarang. Kemudian kita perlu buat file decrypt yang dapat decrypt file tersebut.

Format text dengan dapat dibuat dengan command berikut.

`format=$(date +"%H:%M %d:%m:%Y").txt`

Pertama-tama, kita dapat copy dulu file di `var/log/syslog` ke file `temp.txt` (Karena `var/log/syslog` merupakan file khusus admin, user harus mendapatkan akses admin terlebih dahulu baru script ini bisa jalan). Setelah itu kita encrypt file tersebut.


```
cp /var/log/syslog /home/$cur_user/Desktop/temp.txt
chmod 777 /home/$cur_user/Desktop/temp.txt

```

Command diatas akan mengcopy file syslog ke temp.txt di desktop.

Setelah itu kita dapat mengambil jam sekarang sebagai variabel untuk mengenkripsi file nantinya. 

`rotate=$(date +"%-H")` 

Kita menggunakan `"%-H"` dan bukan `"%H"` agar dapat menghilangkan nol di awal.

Kemudian kita dapat menggunakan fungsi `tr` yang berupa fungsi untuk melakukan tranformasi text. Dalam hal ini fungsi tersebut akan kita gunakan untuk mengenkripsi temp.txt.


```
# String untuk manipulasi
low=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
upp=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ

# Manipulasi string berdasarkan jam kemudian masukin ke file dengan format nama yang ditentukan
tr "${low:0:26}" "${low:${rotate}:26}" < /home/$cur_user/Desktop/temp.txt | tr "${upp:0:26}" "${upp:${rotate}:26}" > /home/$cur_user>
```

`low` dan `upp` adalah variabel yang kita akan kita pakai untuk fungsi `tr` nanti.


`tr "${low:0:26}" "${low:${rotate}:26}" < /home/$cur_user/Desktop/temp.txt ` akan mengenkripsi temp.txt sesuai dengan instruksi dari yang diberikan oleh soal. Tapi enkripsi tersebut baru berupa huruf kecil. Untuk dapat mengenkripsi huruf besar, kita dapat melakukan pipe hasil enkripsi huruf kecil tersebut dengan command tr selanjutnya.

`tr "${upp:0:26}" "${upp:${rotate}:26}" > /home/$cur_user/Desktop/"$format"` 

Command tersebut akan mengenkripsi semua huruf besar di temp.txt tadi. Kemudian setelah itu kita akan memasukkan hasil enkripsi ke file baru di dekstop dengan format yang sudah kita buat tadi.


Untuk cronjobnya kita set untuk backup setiap 2 jam seperti berikut. 

`0 */2 * * * /home/charles/Desktop/log_encrypt.sh`

Setelah itu file baru akan muncul setiap 2 jam dengan isi berupa log yang sudah di enkripsikan dan dengan format nama yang sudah ditentukan tadi. 

Untuk mendekripsikan file tersebut prosesnya cukup sama. Pertama kita ambil dulu jam dari file yang mau di dekripsi

```
# Nama file yang mau di decrypt
filename="16:20 03:03:2023.txt"

# Ambil jam dari nama file
hour=$(echo $filename | cut -d ':' -f 1)
```

Untuk ambil jam, kita dapat menggunakan command `cut -d ':' -f 1` yang akan memisah `filename` berdasarkan `:`. `-f 1` akan mengambil string pertama hasil pemisahan tersebut.

Setelah itu kita dapat menjalankan proses yang sama dengan encrypt tadi, tapi rotatenya berupa kebalikan dari encrypt.


```
# Karena decrypt, kebalikan dari encrypt rotasinya
rotate=26-$hour

# String untuk manipulasi
low=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
upp=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ

# Decrypt file kemudian masukin ke file baru
tr "${low:0:26}" "${low:${rotate}:26}" < /home/$USER/Desktop/"$filename" | tr "${upp:0:26}" "${upp:${rotate}:26}" > /home/$USER/Desktop/"Decrypt $filename"

```

Setelah itu selesai, file berhasil di dekripsikan.

Hasil run : 

Ingat, agar file dapat di run, perlu mengubah file log_encrypt.sh menjadi executable file terlebih dahulu. Hal tersebut dapat dilakukan dengan menjalankan command

`chmod +x log_encrypt.sh`

Agar mudah melakukan test run, di test ini cronjob di ubah menjadi `* * * * * /home/charles/Desktop/log_encrypt.sh` yang mana akan menjalankan log_encrypt.sh setiap menit.

Enkripsi

![hasil enkripsi](soal4/hasil%20encrypt.png)

Bisa dilihat enkripsi dilakukan saat pukul `16.20`. Sehingga kita mengambil `16` sebagai rotasi kita. Jadi semua karakter akan diganti dengan karakter tersebut ditambah `16`.

`Contoh : f akan menjadi v karena f = 5 dan ketika ditambah 16 menjadi 21 yang berupa v`

Dekripsi

![hasil dekripsi](soal4/hasil%20decrypt.png)

Bisa dilihat hasil dekripsi menyebabkan file enkripsi tadi berubah menjadi file biasa lagi.







 
