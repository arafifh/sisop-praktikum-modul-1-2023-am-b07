#!/bin/bash
# Nama : Charles
# NRP : 5025211082
# Filename perlu dimasukkan sendiri

# Nama file yang mau di decrypt
filename="16:20 03:03:2023.txt"

# Ambil jam dari nama file
hour=$(echo $filename | cut -d ':' -f 1)

# Karena decrypt, kebalikan dari encrypt rotasinya
rotate=26-$hour

# String untuk manipulasi
low=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
upp=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ

# Decrypt file kemudian masukin ke file baru
tr "${low:0:26}" "${low:${rotate}:26}" < /home/$USER/Desktop/"$filename" | tr "${upp:0:26}" "${upp:${rotate}:26}" > /home/$USER/Desktop/"Decrypt $filename"

