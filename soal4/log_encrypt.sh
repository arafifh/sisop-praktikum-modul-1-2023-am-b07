#!/bin/bash
# Nama : Charles
# NRP : 5025211082
# Asumsi script berada pada desktop.
# cronjob yang dilakukan :
# 0 */2 * * * /home/charles/Desktop/log_encrypt.sh

# Mendapatkan nama current user
cur_user=$(whoami)

# Format nama
format=$(date +"%H:%M %d:%m:%Y").txt

# Copy log ke dalam temp.txt dan ubah permission
cp /var/log/syslog /home/$cur_user/Desktop/temp.txt
chmod 777 /home/$cur_user/Desktop/temp.txt

# Ambil jam sekarang
rotate=$(date +"%-H")

# String untuk manipulasi
low=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
upp=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ

# Manipulasi string berdasarkan jam kemudian masukin ke file dengan format nama yang ditentukan
tr "${low:0:26}" "${low:${rotate}:26}" < /home/$cur_user/Desktop/temp.txt | tr "${upp:0:26}" "${upp:${rotate}:26}" > /home/$cur_user/Desktop/"$format"

# Hapus temp.txt
rm /home/$cur_user/Desktop/temp.txt
